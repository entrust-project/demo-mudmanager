#!/bin/bash

# Log from new user connection - "Testing to ensure that the correct data has been received."
#echo New commit from IP: $1 with MAC: $2 and with MUD URL: $3 >> /etc/dhcp/mud/leases.log

# Send IP, MAC and MUD-URL to MUD Manager
python3 /etc/dhcp/mud/mudManager.py $1 $2 $3
