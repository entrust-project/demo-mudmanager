# MUD-Manager - Testbed V0.1

This is a open-source proof-of-concept (PoC) intended to help engineers and users to familiarise with the MUD standard. Therefore, the implemen-tation does not include all the mechanisms typical of MUD environments.

## Introduction

Manufacturer Usage Description (MUD) is a technique whereby constrained end devices (e.g., IoT devices) can signal to the network what sort of access and network functionality they require to properly function. The end device performs this signaling by issuing a URL in LLDP, DHCP, or as part of an X.509 certificate. A MUD Manager is a service sitting in the network that receives the MUD URL, fetches a MUD file containing access requirements provided by a manufacturer, and creates Access Control Lists (ACLs) that can be installed on network equipment to allow that access.

The MUD specification can be found in (https://tools.ietf.org/html/draft-ietf-opsawg-mud-25), which has been approved to be an IETF RFC.  This implementation supports all abstractions, except model.  In addition, source and destination IPv4 and IP networks from the ACL model are supported, so long as they are multicast addresses.

After you have installed the MUD Manager, you need to create a MUD file and prepare a device to emit a URL to a MUD file.

An example MUD URL is "https://mudfileserver/mudfile.json," indicating the use of TLS to fetch the file.

## How this MUD Manager testbed works?

Starts when a device joins a certain network, in our case via DHCP, each device in its DHCP request sends a DHCP option MUD URL, this option contains the URL where the device MUD file is located.

This URL is captured by the DHCP server, which sends this URL to the MUD Manager, this Manager will request the MUD file which is signed by the certification authority who tests and build this MUD file, The MUD Manager verifies this signature, and if is OK then MUD Manager proceed with the MUD file translation.

## Components and technologies

* Ubuntu 23.04 (Linux)
* ISC DHCP Server
* ISC DHCP Client
* Notifier (Bash)
* MUD Manager (Python)
* MUD Translator (Python)
* MUD File Server (Docker)

## Installation

### ISC DHCP Server

Install ISC DHCP Server:

```
sudo apt install isc-dhcp-server
```

>[!NOTE]
>To skip this configuration step, download the files from this repository and place them in their respective directories. Provide the necessary permissions using `chmod 644` for `/etc/dhcp/dhcpd.conf` and `/etc/apparmor.d/usr.sbin.dhcpd`, for `notifier.sh` we need use `chmod 755`.

Set up a static IP on same subnet to the interface you will use for the DHCP Service by writing to `netplan` or `/etc/network/interfaces` files or where ever your system has this configuration

Then add the mudurl option to the dhcpd.conf at `/etc/dhcp/dhcpd.conf`:

```
option mudurl code 161 = text;
```

Define your subnet configuration, in our case:

```
subnet 10.0.0.0 mask 255.255.255.0 {
  range 10.0.0.5 10.0.0.255;
}
```

Next add the on commit section to execute the `notifier` when device joins to the network successfully:

```
on commit{
  set ClientIP = binary-to-ascii(10,8,".",leased-address);
  set ClientMac = binary-to-ascii(16,8,":", substring(hardware,1,16));
  log(concat("Commit: IP: ", ClientIP, " MAC: ", ClientMac, " MUD-URL: ", option mudurl));
  execute("/etc/dhcp/notifier.sh", ClientIP, ClientMac, option mudurl);
}
```

>[!WARNING]
>If your system is running AppArmor, you need to add permissions for `notifier`, `mudManager` and other files... 
>You can add the following line to final of `/etc/apparmor.d/usr.sbin.dhcpd`:

```
/etc/dhcp/notifier.sh rix,
```

Now copy the script `notifier.sh` to `/etc/dhcp/` and add permissions to the execution:

```
sudo cp notifier.sh /etc/dhcp/
sudo chmod +x /etc/dhcp/notifier.sh
```

For a simple configuration, you can download `usr.sbin.dhcpd` file from this repository, this file adds all the permissions needed to run this demo.

Finally we can restart our DHCP service:

```
sudo systemctl restart isc-dhcp-server
```

### ISC DHCP Client

Install ISC DHCP Client [it must be installed on a different computer of DHCP server].

```
sudo apt install isc-dhcp-client
```

To configure DHCP client with the MUD-URL in the DHCP request, we need to edit the `/etc/dhcp/dhclient.conf` file, add the following options to the file:

```
option mudurl code 161 = text;
send mudurl "http or https://mudfileserver/mudfile.json";
```

Finally, we force the client to request an IP by executing the command `dhclient`` with the interface name:

```
sudo dhclient eth0
```

### Dependencies

Before running the MUD Manager on our system, we need to install the following packages:

#### Python
We must have Python 3.11 or a later version installed. You can check the version with the following command:

```
python --version
```
or

```
python3 --version
```

If Python is not installed on our system, we can execute the following command:

```
sudo apt install python3
```

#### OpenSSL
OpenSSL is used for cryptographic services, and is available on most Linux systems. If not, then a recent release will need to be installed. It may be available using a package installer (such as apt-get), else it can be downloaded from https://www.openssl.org. 

Check if OpenSSL is already installed on your system:

```
openssl version
```

If not, install the latest version with the following command:

```
sudo apt install openssl
```

### MUD Manager and Translator

MUD Manager is composed by 2 services `mudManager.py` and `mudTranslator.py`. Both need to be placed in the same directory.

For a better organization, we will create three new directories:

```
sudo mkdir /etc/dhcp/mud
sudo mkdir /etc/dhcp/mud/certs
sudo mkdir /etc/dhcp/mud/mudfiles
```

In the first diretory, we place both scripts to run `mudManager.py`and `mudTranslator.py`:

```
sudo cp mudManager.py /etc/dhcp/mud/
sudo cp mudTranslator.py /etc/dhcp/mud/
```

In the second directory, we place the certificates for mud verification.

The last directory, is only to store the mudfiles and signatures downloaded from the MUD-URL requests, but we need to change the owner:

```
sudo chown dhcpd:dhcpd /etc/dhcp/mud/mudfiles
```

### Signing & Verifying MUD Files

According to RFC 8520, MUD files MUST be signed using Cryptographic Message Contents (CMS). Within the MUD file itself, the mud-signature property points to the location where the (detached) signature can be found. The mud-signature property can be used by a MUD Manager to retrieve the signature file. By default, the assumption is that the location of the signature file is right next to the MUD file itself, but it can be somewhere different. A small caveat is that the location of the signature file should be set before signing the MUD file, because it's in the contents of the MUD file to be signed.

A small utility script for generating the keys and certificates for signing a MUD File has been included in this repository.
It serves as an example; it probably shouldn't be used as is for production deployments.

It can be used as follows:

```bash
# within the cert directory:
./certs.sh
```

The certificate and key generated are directly under the newly generated CA, so no intermediates are included.
The command for signing a MUD File using a key and certificate generated with the utility script should thus be changed to not include the intermediate certificate like below:

```bash
# within the mud-file-server repository, assuming example certificates and keys are available
openssl cms -sign -signer server.crt -inkey server.key -in mudfile.json -binary -outform DER -binary -out mudfile.p7s
```

And can be checked as follows:

```bash
# within the mud-file-server repository, assuming example certificates and keys are available
openssl cms -verify -in mudfile.p7s -inform DER -content mudfile.json -binary -CAfile ca.crt -out /dev/null
```

### MUD File Server
MUD File Servers are responsible for serving MUD Files and their signatures, which can be retrieved by MUD Controllers when an IoT device emits a MUD URL.
This repository contains an implementation of a MUD File Server based on the Nginx web server and Docker, what can run on same machine of MUD Manager.

The first thing we need to do is create a new directory where we can place mudfiles and their signatures.

```
sudo mkdir /home/MUDFileServer
```

Now we will check if we have Docker installed:

```
docker version
```

If this is not the case, we have to install with the following commands:

```
sudo apt install docker
sudo apt install docker.io
```

Finally, we can run Docker command to deploy an Nginx web server:

```
docker run --name MUDFileServer -p 80:80 -v /home/MUDFileServer:/usr/share/nginx/html:ro -d nginx
```

Now the MUD File Server can be reached at http://localhost/. Assuming the examples directory is available and the repository directory set as the root to serve files from, the example MUD file can now be retrieved from:

https://localhost/mudfile.json

And its signature can be retrieved from:

https://localhost/mudfile.p7s

## Testing

A simple test can be run if all steps have been followed. When a new device is connected to the network, it sends a mud-url request to the DHCP server. This request is received by the notifier and sent to MUD Manager, which verifies the URL and starts the process to extract policies.

We can check if the execution was correct with the following command:

```
sudo systemctl status isc-dhcp-server
```

The output should look something like this:

```
name:mud-55052-v6to
name: mud-55052-v6fr
acl 0 name: mud-55052-v6to , type: ipv6-acl-type
	ace  0 {'name': 'ent0-todev', 'matches': {'ietf-mud:mud': {'my-controller': [None]}, 'ipv6': {'protocol': 17}, 'udp': {'source-port': {'operator': 'eq', 'port': 5683}}}, 'actions': {'forwarding': 'accept'}}
response IPV6 {'srcnetwork': None, 'dstnetwork': None, 'srcdnsname': None, 'dstdnsname': None, 'protocol': 17}
response UDP {'srcoperator': 'eq', 'srcport': '5683', 'dstoperator': None, 'dstport': None, 'protocoltype': None, 'length': None}

acl 1 name: mud-55052-v6fr , type: ipv6-acl-type
	ace  0 {'name': 'ent0-frdev', 'matches': {'ietf-mud:mud': {'same-manufacturer': [None]}, 'ipv6': {'protocol': 17}, 'udp': {'destination-port': {'operator': 'eq', 'port': 5683}}}, 'actions': {'forwarding': 'accept'}}
response IPV6 {'srcnetwork': None, 'dstnetwork': None, 'srcdnsname': None, 'dstdnsname': None, 'protocol': 17}
response UDP {'srcoperator': None, 'srcport': None, 'dstoperator': 'eq', 'dstport': '5683', 'protocoltype': None, 'length': None}
```

## TODO

- Implement a Threat MUD?
- Translate extracted policies to MSPL?
- Implement a Kafka service?
- Implement with Docker?
- Implement a simple overview page of MUDs available?
- Implement basic statistics about files requested?
- Add signature verification before serving the MUD file (if signature exists; on same server, or a different one)?

