import argparse
import json

#eth matches fields
ETH = "eth"
DESTINATION_MAC = "destination-mac-address"
DESTINATION_MAC_MASK = "destination-mac-address-mask"
SOURCE_MAC = "source-mac-address"
SOURCE_MAC_MASK = "source-mac-address-mask"
ETHERTYPE = "ethertype"
ethfields = [DESTINATION_MAC, DESTINATION_MAC_MASK, SOURCE_MAC, SOURCE_MAC_MASK, ETHERTYPE]

#ipv4 fields
IPV4 = "ipv4"
IPV4_DSCP = "dscp"
IPV4_ECN = "ecn"
IPV4_LENGHT = "lenght"
IPV4_TTL = "ttl"
IPV4_PROTOCOL = "protocol"
IPV4_IHL = "ihl"
IPV4_FLAGS = "flags"
IPV4_OFFSET = "offset"
IPV4_IDENTIFICATION = "identification"
IPV4_DESTINATION_NETWORK = "destination-ipv4-network"
IPV4_SOURCE_NETWORK = "source-ipv4-network"
# module: ietf-acldns
#      augment /acl:acls/acl:acl/acl:aces/acl:ace/
#        acl:matches/acl:l3/acl:ipv4/acl:ipv4:
IPV4_SRC_DNSNAME = "src-dnsname"
IPV4_SRC_IETF_DNSNAME = "ietf-acldns:src-dnsname"
IPV4_DST_DNSNAME = "dst-dnsname"
IPV4_DST_IETF_DNSNAME = "ietf-acldns:dst-dnsname"
ipv4fields = [IPV4_DSCP, IPV4_ECN, IPV4_LENGHT, IPV4_TTL, IPV4_PROTOCOL, IPV4_IHL, IPV4_FLAGS, IPV4_OFFSET, IPV4_IDENTIFICATION,\
               IPV4_DESTINATION_NETWORK, IPV4_SOURCE_NETWORK, IPV4_SRC_DNSNAME, IPV4_DST_DNSNAME, IPV4_SRC_IETF_DNSNAME, IPV4_DST_IETF_DNSNAME, "src-address", "dst-address"]

#ipv6_fields
IPV6 = "ipv6"
IPV6_DSCP = "dscp"
IPV6_ECN = "ecn"
IPV6_LENGHT = "lenght"
IPV6_TTL = "ttl"
IPV6_PROTOCOL = "protocol"
IPV6_DESTINATION_NETWORK = "destination-ipv6-network"
IPV6_SOURCE_NETWORK = "source-ipv6-network"
IPV6_FLOW_LABEL = "flow-label"
# augment /acl:acls/acl:acl/acl:aces/acl:ace/
#        acl:matches/acl:l3/acl:ipv6/acl:ipv6:
IPV6_SRC_DNSNAME = "src-dnsname"
IPV6_SRC_IETF_DNSNAME = "ietf-acldns:src-dnsname"
IPV6_DST_DNSNAME = "dst-dnsname"
IPV6_DST_IETF_DNSNAME = "ietf-acldns:dst-dnsname"
ipv6fields = [IPV6_DSCP, IPV6_ECN, IPV6_LENGHT, IPV6_TTL, IPV6_PROTOCOL, IPV6_DESTINATION_NETWORK, IPV6_SOURCE_NETWORK, IPV6_FLOW_LABEL,\
               IPV6_SRC_DNSNAME, IPV6_DST_DNSNAME, IPV6_SRC_IETF_DNSNAME, IPV6_DST_IETF_DNSNAME]

#tcp fields
TCP = "tcp"
TCP_SEQUENCE_NUMBER = "sequence-number"
TCP_ACKNOWLEDGE_NUMBER = "acknowledgement-number"
TCP_DATA_OFFSET = "data-offset"
TCP_RESERVED = "reserved"
TCP_FLAGS = "flags"
TCP_WINDOW_SIZE = "window-size"
TCP_URGENT_POINTER = "urgent-pointer"
TCP_OPTIONS = "options"
TCP_OPERATOR = "operator"
TCP_PORT = "port"
TCP_SOURCE_PORT = "source-port"
TCP_RANGE_LOWER_PORT = "lower-port"
TCP_RANGE_UPPER_PORT = "upper-port"
TCP_DESTINATION_PORT = "destination-port"
#augment /acl:acls/acl:acl/acl:aces/acl:ace/acl:matches
# /acl:l4/acl:tcp/acl:tcp:
TCP_DIRECTION_INITIATED = "direction-initiated"
TCP_IETF_MUD_DIRECTION_INITIATED = "ietf-mud:direction-initiated"
tcpfields = [TCP_SEQUENCE_NUMBER,TCP_ACKNOWLEDGE_NUMBER,TCP_DATA_OFFSET,TCP_RESERVED,TCP_FLAGS,TCP_WINDOW_SIZE,TCP_URGENT_POINTER,TCP_OPTIONS,\
TCP_SOURCE_PORT,TCP_RANGE_LOWER_PORT,TCP_RANGE_UPPER_PORT,TCP_OPERATOR,TCP_PORT,TCP_DESTINATION_PORT, TCP_IETF_MUD_DIRECTION_INITIATED, TCP_DIRECTION_INITIATED]

#udp fields
UDP = "udp"
UDP_LENGTH = "lenght"
UDP_SOURCE_PORT = "source-port"
UDP_RANGE_LOWER_PORT = "lower-port"
UDP_RANGE_UPPER_PORT = "upper-port"
UDP_OPERATOR = "operator"
UDP_PORT = "port"
UDP_DESTINATION_PORT = "destination-port"
udpfields = [UDP,UDP_LENGTH,UDP_SOURCE_PORT,UDP_RANGE_LOWER_PORT,UDP_RANGE_UPPER_PORT,UDP_OPERATOR,UDP_PORT,UDP_DESTINATION_PORT]

#icmp fields
ICMP = "icmp"
ICMP_TYPE = "type"
ICMP_CODE = "code"
ICMP_REST_OF_HEADER = "rest-of-header"

EGRESS_INTERFACE = "egress-interface"
INGRESS_INTERFACE = "igress-interface"

#matches augment
#augment /acl:acls/acl:acl/acl:aces/acl:ace/acl:matches:
MUD = "mud"
MUD_MANUFACTURER = "manufacturer"
MUD_SAME_MANUFACTURER = "same-manufacturer"
MUD_MODEL = "model"
MUD_LOCAL_NETWORKS = "local-networks"
MUD_CONTROLLER = "controller"
MUD_MY_CONTROLLER = "my-controller"

#matches layer { eth, ipv4, ipv6, tcp, udp, icmp }
matcheslayer = [ETH, IPV4, IPV6, TCP, UDP, ICMP]

def parse_args():
    parser = argparse.ArgumentParser(description='parser for mud translation')
    parser.add_argument("-i", "--input")
    args = parser.parse_args()
    return args

def extract_eth_rule(rule):
    response = {"dstmac": None, "dstmacmask": None, "srcmac": None, "srcmacmask": None, "ethtype": None}
    for attribute in rule:
        if not attribute in ethfields:
            return
        if attribute == DESTINATION_MAC:
            response["dstmac"] = rule[attribute]
        elif attribute == DESTINATION_MAC_MASK:
            response["dstmacmask"] = rule[attribute]
        elif attribute == SOURCE_MAC:
            response["srcmac"] = rule[attribute]
        elif attribute == SOURCE_MAC_MASK:
            response["srcmacmask"] = rule[attribute]
        elif attribute == ETHERTYPE:
            response["ethtype"] = rule[attribute]
        else:
            return "not important attribute"
    return response

def extract_ipv4_rule(rule):
    response = {"srcnetwork": None, "dstnetwork": None, "srcdnsname": None, "dstdnsname": None, "protocol": None}
    for attribute in rule:
        if not attribute in ipv6fields:
            return
        if attribute == IPV4_SOURCE_NETWORK:
            response["srcnetwork"] = rule[attribute]
        elif attribute == IPV4_DESTINATION_NETWORK:
            response["dstnetwork"] = rule[attribute]
        elif attribute == IPV4_SRC_IETF_DNSNAME:
            response["srcdnsname"] = rule[attribute]
        elif attribute == IPV4_DST_IETF_DNSNAME:
            response["dstdnsname"] = rule[attribute]
        elif attribute == IPV4_PROTOCOL:
            response["protocol"] = rule[attribute]
        else:
            return "not important attribute " + attribute
    return response

def extract_tcp_rule(rule):
    response = {"srcoperator": None, "srcport": None, "dstoperator": None, "dstport": None, "dirinitiated": None}

    for attribute in rule:
        if not attribute in tcpfields:
            return
        if attribute == TCP_SOURCE_PORT:
            if TCP_OPERATOR or TCP_PORT in rule[attribute]:
                response["srcoperator"] = rule[attribute][TCP_OPERATOR]
                response["srcport"] = str(rule[attribute][TCP_PORT])
        elif attribute == TCP_DESTINATION_PORT:
            if TCP_OPERATOR or TCP_PORT in rule[attribute]:
                response["srcoperator"] = rule[attribute][TCP_OPERATOR]
                response["srcport"] = str(rule[attribute][TCP_PORT])
        elif attribute == TCP_DIRECTION_INITIATED:
            response["dirinitiated"] = rule[attribute]
        else:
            return "not important attribute " + attribute
    return response

def extract_udp_rule(rule):
    response = {"srcoperator": None, "srcport": None, "dstoperator": None, "dstport": None, "protocoltype": None, "length": None }
    for attribute in rule:
        if not attribute in udpfields:
            return
        if attribute == UDP_LENGTH:
            response["length"] = rule[attribute]
        elif attribute == UDP_SOURCE_PORT:
            if UDP_OPERATOR or UDP_PORT in rule[attribute]:
                response["srcoperator"] = rule[attribute][UDP_OPERATOR]
                response["srcport"] = str(rule[attribute][UDP_PORT])
        elif attribute == UDP_DESTINATION_PORT:
            if UDP_OPERATOR or UDP_PORT in rule[attribute]:
                response["dstoperator"] = rule[attribute][UDP_OPERATOR]
                response["dstport"] = str(rule[attribute][UDP_PORT])
        else:
            return "not important attribute " + attribute
    return response

def extract_ipv6_rule(rule):
    response = {"srcnetwork": None, "dstnetwork": None, "srcdnsname": None, "dstdnsname": None, "protocol": None}
    for attribute in rule:
        if not attribute in ipv6fields:
            return
        if attribute == IPV6_SOURCE_NETWORK:
            response["srcnetwork"] = rule[attribute]
        elif attribute == IPV6_DESTINATION_NETWORK:
            response["dstnetwork"] = rule[attribute]
        elif attribute == IPV6_SRC_IETF_DNSNAME:
            response["srcdnsname"] = rule[attribute]
        elif attribute == IPV6_DST_IETF_DNSNAME:
            response["dstdnsname"] = rule[attribute]
        elif attribute == IPV6_PROTOCOL:
            response["protocol"] = rule[attribute]
        else:
            return "not important attribute " + attribute
    return response

def extract_rule(ace, to_device):
    name = ace["name"]
    matches = ace["matches"]

    intersection = [e for e in matcheslayer if e in matches]
    tcp = udp = ipv4 = ipv6 = icmp = False  
    responses = {"eth": None, "ipv4": None, "ipv6": None, "tcp": None, "udp": None, "icmp": None}
    #print("Intersection ", intersection)

    for i in intersection:
        if i == ETH:
            extract_eth_rule(matches[ETH])
        elif i == IPV4:
            response = extract_ipv4_rule(matches[IPV4])
            print("response IPV4", response)
            responses["ipv4"] = response
            ipv4 = True
        if i == IPV6:
            response = extract_ipv6_rule(matches[IPV6])
            print("response IPV6", response)
            responses["ipv6"] = response
            ipv6 = True
        elif i == TCP:
            response = extract_tcp_rule(matches[TCP])
            print("response TCP", response)
            responses["tcp"] = response
            tcp = True
        elif i == UDP:
            response = extract_udp_rule(matches[UDP])
            print("response UDP", response)
            responses["udp"] = response
            udp = True
        elif i == ICMP:
            response = extract_icmp_rule(matches[ICMP])
            print("response ICMP", response)
            responses["icmp"] = response
            icmp = True
        else:
            print("No filter layer type found " + i)
            return

def translate_mud(mud):

    if 'ietf-access-control-list:acls' in mud:
        access_control_list = mud['ietf-access-control-list:acls']
        acls = access_control_list['acl']
        try:
            acl_to_device = mud['ietf-mud:mud']['to-device-policy']['access-lists']['access-list']
        except:
            print("Threat MUD!")
            acl_to_device = mud['ietf-threatmud:mud']['to-device-policy']['access-lists']['access-list']
        
        to_device = []
        for acl in acl_to_device:
            print("name:" + acl['name'])
            to_device.append(acl['name'])
        
        try:
            acl_from_device = mud['ietf-mud:mud']['from-device-policy']['access-lists']['access-list']
        except:
            acl_from_device = mud['ietf-threatmud:mud']['from-device-policy']['access-lists']['access-list']
        
        from_device = []
        for acl in acl_from_device:
            print("name: " + acl['name'])
            from_device.append(acl['name'])
        
        rules = []
        for i, acl in enumerate(acls):
            print("acl", i, "name:",acl['name'], ", type:", acl['type'])
            for index,ace in enumerate(acl['aces']['ace']):
                print("\tace ", index, ace)
                rules.append(extract_rule(ace, acl['name'] in to_device))
            print("")
        print("rules", rules)
    
    else:
        return None
        
def main():
    args = parse_args()
    mudfile = open(args.input)
    mud = json.load(mudfile)
    translate_mud(mud)

if __name__ == "__main__":
    main()
