import sys
import os
import requests
import logging
import json
import subprocess
import mudTranslator
from urllib.parse import urlparse

class mudManager:
    
    #Validates the given MUD URL by checking if it has a valid scheme (http or https)
    def validateURL(self, mud_url):
        try:
            parsed_url = urlparse(mud_url)
            if parsed_url.scheme and parsed_url.scheme.lower() in ["http", "https"]:
                return True
            else:
                return False
        except Exception as e:
            print(f"Error checking URL: {e}")
            return False

    #Fetches the MUD file content from the provided URL
    def getMUDFile(self, mudurl):
        try:
            r = requests.get(mudurl)
            if r.status_code == 200:
                print(f"MUD File fetched")
                return r.text
            else:
                print(f"Error: Unable to fetch MUD file. Status Code: {r.status_code}")
                return None
        except Exception as e:
            print(f"Error: {e}")
            return None
    
    #Downloads the MUD file and its signature, then verifies if it is correct using OpenSSL
    def verifyMUDFile(self, mudurl):
        mudsignurl = mudurl.replace(".json", ".p7s")
        mud_directory = "/etc/dhcp/mud/mudfiles/"

        # Extract the name from the URL
        parsed_url = urlparse(mudurl)
        mudfile_name = os.path.splitext(os.path.basename(parsed_url.path))[0]

        logging.info("Downloading mudfile from: " + mudurl)
        try:
            r = requests.get(mudurl)
            r.raise_for_status()
        except requests.exceptions.RequestException as e:
            logging.error(f"Error downloading MUD file: {e}")
            return False

        mudfile_path = os.path.join(mud_directory, mudfile_name + ".json")
        with open(mudfile_path, "w+") as file:
            file.write(r.text)

        logging.info("Downloading signature for mudfile from: " + mudsignurl)
        try:
            r = requests.get(mudsignurl)
            r.raise_for_status()
        except requests.exceptions.RequestException as e:
            logging.error(f"Error downloading MUD signature: {e}")
            os.remove(mudfile_path)
            return False

        mudcrt_path = os.path.join(mud_directory, mudfile_name + ".p7s")
        with open(mudcrt_path, "wb+") as file:
            file.write(r.content)

        ca_crt_path = "/etc/dhcp/mud/cert/ca.crt"
        logging.info("Verifying MUD File")

        # Use subprocess to run the openssl command for verification
        try:
            cmd = [
                "openssl",
                "cms",
                "-verify",
                "-in", mudcrt_path,
                "-inform", "DER",
                "-content", mudfile_path,
                "-binary",
                "-CAfile", ca_crt_path,
                "-out", "/dev/null"  # Output to null, as we are interested in the return code
            ]
            subprocess.run(cmd, check=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            ret = True  # Verification successful
        except subprocess.CalledProcessError:
            logging.error("MUD Verification failed")
            ret = False
        except Exception as e:
            logging.error(f"Error verifying MUD file: {e}")
            ret = False
        finally:
            print("MUD Verified")
            # Uncomment the following lines if you want to remove the files after verification
            # os.remove(mudcrt_path)
            # os.remove(mudfile_path)

        if ret:
            logging.info("MUD Verified")

        return ret

def main():
    try:
        if len(sys.argv) != 4:
            print("Usage: python your_script.py HOSTNAME IP MAC")
            sys.exit(1)

        ip_address = sys.argv[1]
        mac_address = sys.argv[2]
        mud_url = sys.argv[3]

        # Test without other device
        # ip_address = "10.0.0.52"
        # mac_address = "8:0:27:9d:4:3"
        # mud_url = "http://localhost/mudfile.json"

        print(f"Received parameters - IP: {ip_address}, MAC: {mac_address}, MUD-URL: {mud_url}")

        # Instantiate the class
        mudDevice = mudManager()
        if mudDevice.validateURL(mud_url):
            print(f"The URL {mud_url} is valid.")
            mud_content = mudDevice.getMUDFile(mud_url)
            if mud_content:
                mudDevice.verifyMUDFile(mud_url)
                with open("/etc/dhcp/mud/mudfiles/mudfile.json", "r") as file:
                    mudfile = json.load(file)
                print("MUD extracted policies:")
                mudTranslator.translate_mud(mudfile)
        else:
            print(f"The URL {mud_url} is not valid.")
    except Exception as e:
        logging.exception(f"Error in main: {e}")

if __name__ == "__main__":
    main()
